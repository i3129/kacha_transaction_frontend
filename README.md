Kacha Transaction Frontend


1. Description
Kacha Transaction Frontend is a React application designed to interact with the Kacha Transaction Backend.
It provides a user interface for creating and viewing transactions.

2. Features
User-friendly interface for managing transactions

Integration with Kacha Transaction Backend for data handling

3. Requirements
Node.js
npm (Node Package Manager)

4. Installation
Clone the repository: git clone https://gitlab.com/i3129/kacha_transaction_frontend.git

Navigate to the project directory: cd Kacha_Transaction_Frontend

Install dependencies: npm install

5. Configuration
Ensure the Kacha Transaction Backend is running and accessible from the frontend. 

6. Usage
Start the frontend server: npm start
Access the application in your web browser at http://localhost:3000
Use the interface to create and view transactions.

7. Components
Form.js: Component for creating new transactions

8. Code is in master branch
